import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';
import { TranslateService } from './../../providers/translate';
import { CommonService } from './../../providers/common';
import { ApiService } from './../../providers/api';

import { Transaction } from '../../modal/transaction-modal';
import { BillFrom } from '../../modal/bill-from-modal';
import { BillTo } from '../../modal/bill-to-modal';
import { Item } from '../../modal/item-modal';
import { Transport } from '../../modal/transport-modal';

@IonicPage()
@Component({
  selector: 'page-add-bill',
  templateUrl: 'add-bill.html',
})
export class AddBillPage {

  transaction: Transaction = {};
  billFrom: BillFrom = {};
  billTo: BillTo = {};
  items: Item = {};
  transport: Transport = {};

  transactionDetails: boolean = false;
  billFromDetails: boolean = true;
  billToDetails: boolean = true;
  itemsDetails: boolean = true;
  transportDetails: boolean = true;

  itemsList = [];
  states = [];

  fromStateCode: any = 0;
  toStateCode: any = 0;

  startAddress: any = '';
  endAddress: any = '';

  transaction_type: any = '';
  transaction_sub_type: any = '';
  document_type: any = '';

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController,public alertCtrl: AlertController, public translate: TranslateService, public api: ApiService, public common: CommonService) {
    this.getStates();
  }

  showTransactionDetails(){
    this.transactionDetails = !this.transactionDetails;
  }

  showBillFromDetails(){
    this.billFromDetails = !this.billFromDetails;
  }

  showBillToDetails(){
    this.billToDetails = !this.billToDetails;
  }

  showItemDetails(){
    this.itemsDetails = !this.itemsDetails;
  }

  showTransportDetails(){
    this.transportDetails = !this.transportDetails;
  }

  addTransaction(){
    let data = {
      header_text: 'Add Transaction',
      mode: 1
    };
    let addTransactionModal = this.modalCtrl.create('AddTransactionPage', data);
    addTransactionModal.onDidDismiss(res => {
      this.transaction = res;
      if(res['transaction_type']=='O'){
        this.common.selectedBillFrom = this.common.selectedBusiness;
        this.common.selectedBillTo = null;
        this.transaction_type = 'Outward-Supply';
      }
      else if(res['transaction_type']=='I'){
        this.common.selectedBillTo = this.common.selectedBusiness;
        this.common.selectedBillFrom = null;
        this.transaction_type = 'Inward-Supply';
      }
      this.transactionSubType(this.transaction.transaction_sub_type);
      this.documentType(this.transaction.document_type);
      console.log(this.transaction);
    });
    addTransactionModal.present();
  }

  addBillFrom(){
    let data = {
      header_text: 'Add Bill From',
      mode: 1
    };
    let addTransactionModal = this.modalCtrl.create('AddBillFromPage', data);
    addTransactionModal.onDidDismiss(res => {
      this.billFrom = res;
      this.fromStateCode = res.bill_from_state;
      this.startAddress = `${res.bill_from_place}, ${res.bill_from_pincode}`;
    });
    addTransactionModal.present();
  }

  addBillTo(){
    let data = {
      header_text: 'Add Bill To',
      mode: 1
    };
    let addTransactionModal = this.modalCtrl.create('AddBillToPage', data);
    addTransactionModal.onDidDismiss(res => {
      this.billTo = res;
      this.toStateCode = res.bill_to_state;
      this.endAddress = `${res.bill_to_place}, ${res.bill_to_pincode}`;
    });
    addTransactionModal.present();
  }

  addItem(){
    let data = {
      header_text: 'Add Item',
      mode: 1
    };
    let addTransactionModal = this.modalCtrl.create('AddItemPage', data);
    addTransactionModal.onDidDismiss(res => {
      if(res.item_description!=''){

        let data = {
          cessAdvol:null,
          cessAmt:null,
          cessrate:null,
          cgst:null,
          cgstRate:null,
          description:null,
          hsnSac:null,
          igst:null,
          igstRate:null,
          inv_no:null,
          itemDescription:null,
          item_id:null,
          qty:null,
          rate:null,
          sgst:null,
          sgstRate:null,
          taxablerate:null,
          taxablevalue1:null,
          total:null,
          unitofmeasurement:null
        }

        data.item_id = res.item_id;
        data.inv_no = res.inv_no;
        data.itemDescription = res.itemDescription;
        data.description = res.itemDescription;
        data.hsnSac = res.hsnSac;
        data.qty = res.qty;
        data.unitofmeasurement = res.unitofmeasurement;

        data.rate = res.rate;
        data.taxablerate = res.taxablerate;
        data.cessrate = res.cessrate;

        let qty = parseFloat(res.qty);
        let tax_rate = parseFloat(res.taxablerate);
        let rate = parseFloat(res.rate);
        let tax_price = rate*qty*tax_rate/100;
        let cess_rate = parseFloat(res.cessrate);

        if(this.fromStateCode==this.toStateCode)
        {
          data.cgst = tax_rate/2;
          data.cgstRate = tax_price/2;

          data.sgst = tax_rate/2;
          data.sgstRate = tax_price/2;

          data.igst = 0;
          data.igstRate = 0;
        }
        else
        {
          data.igst = tax_rate;
          data.igstRate = tax_price;

          data.cgst = 0;
          data.cgstRate = 0;
          data.sgst = 0;
          data.sgstRate = 0;
        }

        data.cessAmt = (rate*qty)*cess_rate/100;
        data.cessAdvol = (rate*qty)*cess_rate/100;
        data.taxablevalue1 =  rate*qty;
        data.total = rate*qty*(100+tax_rate)/100 + (rate*qty)*cess_rate/100;

        this.itemsList.push(data);
      }
    });
    addTransactionModal.present();
  }

  updateItem(item){
    let data = {
      header_text: 'Update Item',
      mode: 2,
      itemData: item
    };
    let addTransactionModal = this.modalCtrl.create('AddItemPage', data);
    addTransactionModal.onDidDismiss(res => {
      (this.itemsList).forEach(element => {
        if(element.item_id==res.item_id){

          element.item_id = res.item_id;
          element.inv_no = res.inv_no;
          element.itemDescription = res.itemDescription;
          element.description = res.itemDescription;
          element.hsnSac = res.hsnSac;
          element.qty = res.qty;
          element.unitofmeasurement = res.unitofmeasurement;

          element.rate = res.rate;
          element.taxablerate = res.taxablerate;
          element.cessrate = res.cessrate;

          let qty = parseFloat(res.qty);
          let tax_rate = parseFloat(res.taxablerate);
          let rate = parseFloat(res.rate);
          let tax_price = rate*qty*tax_rate/100;
          let cess_rate = parseFloat(res.cessrate);

          if(this.fromStateCode==this.toStateCode)
          {
            element.cgst = tax_rate/2;
            element.cgstRate = tax_price/2;

            element.sgst = tax_rate/2;
            element.sgstRate = tax_price/2;

            element.igst = 0;
            element.igstRate = 0;
          }
          else
          {
            element.igst = tax_rate;
            element.igstRate = tax_price;

            element.cgst = 0;
            element.cgstRate = 0;
            element.sgst = 0;
            element.sgstRate = 0;
          }

          element.cessAmt = (rate*qty)*cess_rate/100;
          element.cessAdvol = (rate*qty)*cess_rate/100;
          element.taxablevalue1 =  rate*qty;
          element.total = rate*qty*(100+tax_rate)/100 + (rate*qty)*cess_rate/100;

          console.log(element);
        }
      });
    });
    addTransactionModal.present();
  }

  deleteItem(item){
    const alert = this.alertCtrl.create({
      title: 'Confirm',
      message: 'Your item will be removed from list.',
      buttons: [
        {
          text: 'No',
          cssClass: 'cancelAlertButton',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          cssClass: 'submitAlertButton',
          handler: data => {
            (this.itemsList).forEach(element => {
              let index = this.itemsList.indexOf(item);
              if (index > -1) {
                this.itemsList.splice(index, 1);
              }
            });
          }
        }
      ]
    });
    alert.present();
  }

  addTransport(){
    let data = {
      header_text: 'Add Transport',
      startAddress: this.startAddress,
      endAddress: this.endAddress,
      mode: 1
    };
    let addTransportModal = this.modalCtrl.create('AddTransportPage', data);
    addTransportModal.onDidDismiss(res => {
      this.transport = res;
      console.log(this.transport);
    });
    addTransportModal.present();
  }

  save(){
    let docDate = new Date(this.transaction.document_date);
    let transDocDate = new Date(this.transport.transportation_date);

    if(this.transaction.transaction_type=='' || this.billFrom.bill_from_name=='' || this.billTo.bill_to_name=='' || this.itemsList.length==0 || this.transport.transportion_name==''){
      this.common.displayToaster('Please fill all form correctly!');
      return false;
    }
    if(docDate.getTime()>transDocDate.getTime()){
      this.common.displayToaster('Transport date must be less than doc date!');
      return false;
    }

    let totalCgst = 0;
    let totalSgst = 0;
    let totalIgst = 0;
    let total_cess_value = 0;
    let total_taxable_value = 0;
    let total_invoice_value = 0;

    (this.itemsList).forEach((element)=>{

      let qty = parseFloat(element.qty);
      let tax_rate = parseFloat(element.taxablerate);
      let rate = parseFloat(element.rate);
      let tax_price = (rate*qty)*tax_rate/100;
      let cess_rate = parseFloat(element.cessrate);

      if(this.billFrom.bill_from_state==this.billTo.bill_to_state)
      {
        element.cgst = tax_rate/2;
        element.cgstRate = tax_price/2;
        totalCgst = totalCgst + tax_price/2;

        element.sgst = tax_rate/2;
        element.sgstRate = tax_price/2;
        totalSgst = totalSgst + tax_price/2;

        element.igst = 0;
        element.igstRate = 0;
      }
      else
      {
        element.igst = tax_rate;
        element.igstRate = tax_price;
        totalIgst = totalIgst + tax_price;

        element.cgst = 0;
        element.cgstRate = 0;
        element.sgst = 0;
        element.sgstRate = 0;
      }
      total_cess_value = total_cess_value + (rate*qty)*cess_rate/100;
      total_taxable_value = total_taxable_value + rate*qty;
      element.total = rate*qty*(100+tax_rate)/100 + (rate*qty)*cess_rate/100;
      total_invoice_value = total_invoice_value + element.total;
    });

    let fromState = null;
    let toState = null;

    this.states.forEach((element)=>{
      if(element.state_code==this.billFrom.bill_from_state){
        fromState = element.state_name;
      }
      if(element.state_code==this.billTo.bill_to_state){
        toState = element.state_name;
      }
    });

    let data = {
      userid: this.common.loginData.ID,
      gstinid: this.common.selectedBusiness.GSTIN.gstinid,
      buid: this.common.selectedBusiness.buid,
      inv_no: 0,

      supplyType: this.transaction.transaction_type,
      subSupplyType: this.transaction.transaction_sub_type,
      supplyTypeDesc: null,
      othersubtype: null,
      subSupplyType1:null,
      docType: this.transaction.document_type,
      docType1: null,
      docNo: this.transaction.document_no,
      docDate: this.common.formatDate(this.transaction.document_date),

      fromGstin: this.billFrom.bill_from_gstin,
      fromSupply: this.billFrom.bill_from_state_code,
      fromSupplyDesc: null,
      fromTrdName: this.billFrom.bill_from_name,
      fromAddr1: this.billFrom.bill_from_address_one,
      fromAddr2: this.billFrom.bill_from_address_two,
      fromPlace: this.billFrom.bill_from_place,
      fromPincode: this.billFrom.bill_from_pincode,
      fromStateCode: this.billFrom.bill_from_state,

      toGstin: this.billTo.bill_to_gstin,
      toSupply: this.billTo.bill_to_state_code,
      toSupplyDesc: null,
      toTrdName: this.billTo.bill_to_name,
      toAddr1: this.billTo.bill_to_address_one,
      toAddr2: this.billTo.bill_to_address_two,
      toPlace: this.billTo.bill_to_place,
      toPincode: this.billTo.bill_to_pincode,
      toStateCode: this.billTo.bill_to_state,

      totalValue: total_taxable_value,
      cgstValue: totalCgst,
      sgstValue: totalSgst,
      igstValue: totalIgst,
      cessValue: total_cess_value,
      totalTaxableValue: total_invoice_value,

      transporterId: null,
      transporterName: this.transport.transportion_name,
      transDocNo: this.transport.transport_doc_no,
      transMode: this.transport.transportation_mode,
      transModeDesc: null,
      transDistance: this.transport.approx_distance,
      transDocDate: this.common.formatDate(this.transport.transportation_date),

      vehicleNo: this.transport.vehicle_number,
      vehicleType: this.transport.vehicle_type,
      isaction: 'I',
      ewbapiNo: null,
      ewbapiDate: null,
      validUpto: null,
      fromStateName: fromState,
      toStateName: toState,
      userGstin: this.common.selectedBusiness.GSTIN.gstin,
      generatedName: null,
      generatedGstin: null,
      isstatus: null,
      rejectStatus: null,

      itemList: this.itemsList,
      vehicleDetaillist: null
    }

    console.log(data);
    this.common.displayLoader('Please wait...');
    this.api.post('add-ewb-bill', data).subscribe((res)=>{
      console.log(res);
      if(res['status']==false){
        this.common.displayToaster(res['message']);
      }
      else{
        this.common.displayToaster('Bill Saved Successfully!');
        this.navCtrl.pop();
      }
      this.common.hideLoader();
    },(err)=>{
      console.log(err);
      this.common.displayToaster('Oops, Something went wrong!');
      this.common.hideLoader();
    });
  }

  getStates(){
    this.api.get('getstate').subscribe((res)=>{
      this.states = JSON.parse(atob(res['stateList']));
    }, (err)=>{
      console.log(err);
      this.common.displayToaster('Oops, Something went wrong!');
    });
  }

  transactionSubType(value: any){
    let transactionSubTypeList = [
      {value:2, name: 'Import'},
      {value:12, name: 'Exhibition or Fairs'},
      {value:3, name: 'Export'},
      {value:5, name: 'For Own Use'},
      {value:4, name: 'Job Work'},
      {value:10, name: 'Line Sales'},
      {value:8, name: 'Others'},
      {value:11, name: 'Recepient Not Known'},
      {value:9, name: 'SKD/CKD'},
      {value:1, name: 'Supply'},
      {value:6, name: 'Job work Returns'},
      {value:7, name: 'Sales Return'},
      {value:8, name: 'Others'},
      {value:1, name: 'Supply'}
    ];
    transactionSubTypeList.forEach((element)=>{
      if(element.value==value){
        this.transaction_sub_type = element.name;
      }
    });
  }

  documentType(value: any){
    let documentTypeList = [
      {value:'BOE', name: 'Advance Receipt'},
      {value:'BIL', name: 'Bills of Supply'},
      {value:'CNT', name: 'Credit Note'},
      {value:'CHL', name: 'Delivery Challan'},
      {value:'OTH', name: 'Other'},
      {value:'INV', name: 'Sales Invoice'},
      {value:'INV', name: 'Purchase Invoice'}
    ];
    documentTypeList.forEach((element)=>{
      if(element.value==value){
        this.document_type = element.name;
      }
    });
  }

}
