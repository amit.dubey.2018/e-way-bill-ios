import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from './../../providers/translate';
import { CommonService } from './../../providers/common';
import { ApiService } from './../../providers/api';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  email: string = '';
  password: string = '';
  displayPassword: boolean = true;
  hidePassword: boolean = false;
  invalidEmail: boolean = false;
  invalidEmailMessage: any = '';
  invalidPassword: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public translate: TranslateService, public common: CommonService, public api: ApiService) {}

  ionViewDidEnter(){
   let user = localStorage.getItem('loginData');
   if(user){
    this.navCtrl.setRoot('HomePage');
   }
  }

  validateEmail(){
    this.api.validateEmail(this.email).subscribe((res)=>{
      let data = res['email_validation'];
      let code = data.status_code;
      let message = this.common.emailValidationMessage(code);
      if(code==50){
        this.invalidEmailMessage = message;
        this.invalidEmail = false;
      }
      else{
        this.invalidEmail = true;
        this.invalidEmailMessage = message;
      }
    },(err)=>{
      console.log(err);
    });
  }

  login(){
    const emailRegexp = /^[a-zA-Z0-9._%+-]+@[a-zA-Z]+\.[a-zA-Z]{2,4}$/;
    if ((this.email=='' || this.email.length<=5 || !emailRegexp.test(this.email))) {
      this.invalidEmail = true;
      return false;
    }
    else if(this.password=='') {
      this.invalidEmail = false;
      this.invalidPassword = true;
      return false;
    }
    else{
      this.invalidEmail = false;
      this.invalidPassword = false;
      this.common.displayLoader('Please wait...');
      this.api.post('login', {emilId: (this.email).toLowerCase(), password: this.password}).subscribe((res)=>{
        this.common.hideLoader();
        if(res['status']==false){
          this.common.displayToaster(res['message']);
        }
        else{
          this.common.loginData = JSON.parse(atob(res['result']));
          localStorage.setItem('loginData', atob(res['result']));
          this.common.displayToaster('Logged In Successfully!');
          this.navCtrl.setRoot('HomePage');
        }
      },(err)=>{
        console.log(err);
        this.common.displayToaster('Oops, Something went wrong!');
        this.common.hideLoader();
      });
    }
  }

  viewPassword(){
    this.displayPassword = !(this.displayPassword);
    this.hidePassword = !(this.hidePassword);
  }

  register(){
    this.navCtrl.push('SignupPage');
  }

  forgot(){
    this.navCtrl.push('ForgotPasswordPage');
  }

}
