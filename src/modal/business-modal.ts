export interface Business{
  business_name?: string,
	gstin?: string,
  branch?: string,
	state?: string,
	email?: string,
  phone?: number,
  address_one?: string,
  address_two?: string,
  place?: string,
  pincode?: number
}
