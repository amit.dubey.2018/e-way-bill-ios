import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { TranslateService } from './../../providers/translate';
import { CommonService } from './../../providers/common';
import { ApiService } from './../../providers/api';

@IonicPage()
@Component({
  selector: 'page-generated-bill-details',
  templateUrl: 'generated-bill-details.html',
})
export class GeneratedBillDetailsPage {

  billDetails: any;

  transactionDetails: boolean = false;
  billFromDetails: boolean = true;
  billToDetails: boolean = true;
  itemsDetails: boolean = true;
  transportDetails: boolean = true;
  vehicleDetails: boolean = true;
  isPrinting: boolean = false;
  itemsList: any = [];
  vehicleList: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public translate: TranslateService, public api: ApiService, public common: CommonService) {
    this.billDetails = JSON.parse(localStorage.getItem('billDetails'));
    this.itemsList = this.billDetails.itemList;
    this.vehicleList = this.billDetails.vehicleDetaillist;
    console.log(this.billDetails);
  }

  showTransactionDetails(){
    this.transactionDetails = !this.transactionDetails;
  }

  showBillFromDetails(){
    this.billFromDetails = !this.billFromDetails;
  }

  showBillToDetails(){
    this.billToDetails = !this.billToDetails;
  }

  showItemDetails(){
    this.itemsDetails = !this.itemsDetails;
  }

  showTransportDetails(){
    this.transportDetails = !this.transportDetails;
  }

  showVehicleDetails(){
    this.vehicleDetails = !this.vehicleDetails;
  }

  startDriving(){
    let address_one = this.billDetails.toAddr1;
    let address_two = this.billDetails.toAddr2;
    let city = this.billDetails.toPlace;
    let state = this.billDetails.toStateName;
    let pincode = this.billDetails.toPincode;
    let destination = `${address_one}, ${address_two}, ${city}, ${state}, ${pincode}`;
    this.navCtrl.push('DrivingPage', {destination: destination});
  }

  printInvoice(){
    let alert = this.alertCtrl.create({
      title: 'Generate Invoice',
      message: 'Do you want to generate eway bill invoice?',
      buttons: [
        {
          text: 'No',
          cssClass: 'cancelAlertButton',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Yes',
          cssClass: 'submitAlertButton',
          handler: () => {
            this.printEwayBill();
          }
        }
      ]
    });
    alert.present();
  }

  printEwayBill(){
    this.common.displayLoader('Please wait...');
    let userid = btoa(this.common.loginData.ID);
    let buid = btoa(this.common.selectedBusiness.GSTIN.buid);
    let gstinid = btoa(this.common.selectedBusiness.GSTIN.gstinid);
    let inv_no = this.billDetails.inv_no;
    let type = 'V';
    let url = `print-ewb/${userid}/${buid}/${gstinid}/${inv_no}/${type}`;
    this.api.get(url).subscribe((res)=>{
      this.common.hideLoader();
      if(res['status']==false)
      {
        this.common.displayToaster('Sorry, Unable to process request!');
      }
      else
      {
        let invoiceData = JSON.parse(atob(res['invoices']));
        this.navCtrl.push('PrintEwayBillPage', {invoiceData: invoiceData[0]});
      }
    }, (err)=>{
      console.log(err);
      this.common.hideLoader();
      this.common.displayToaster('Oops, Something went wrong!');
    });
  }

}
