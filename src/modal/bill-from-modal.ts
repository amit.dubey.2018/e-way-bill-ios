export interface BillFrom{

  bill_from_name?:string,
  bill_from_gstin?:string,
  bill_from_state?:string,
  bill_from_state_code?:string,
  bill_from_address_one?:string,
  bill_from_address_two?:string,
  bill_from_place?:string,
  bill_from_pincode?:string

}
