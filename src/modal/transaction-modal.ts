export interface Transaction{
  transaction_type?: string,
  transaction_sub_type?: string,
  document_type?: string,
  document_no?: string,
  document_date?: string,
}
