import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { TranslateService } from './../../providers/translate';
import { CommonService } from './../../providers/common';
import { ApiService } from './../../providers/api';
import { Business } from './../../modal/business-modal';

declare var google;

@IonicPage()
@Component({
  selector: 'page-add-business',
  templateUrl: 'add-business.html',
})
export class AddBusinessPage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;

  business: Business = {};
  states: any = [];

  isBusiness: any;
  title_text: any;
  gstinid: any;
  userid: any;
  buid: any;
  action: any;
  gstinCode: any;
  email: any;
  phoneno: any;
  selectedState: any = null;

  address: any;
  options: any;
  mode: any;

  invalidGstin:  boolean = false;
  invalidGstinMessage: any;
  invalidMobile: boolean = false;
  invalidMobileMessage: any;
  invalidPincode: boolean = false;
  invalidPincodeMessage: any;

  constructor(public geolocation: Geolocation, public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public translate: TranslateService, public api: ApiService, public common: CommonService) {

    this.isBusiness = this.navParams.get('isBusiness');
    this.title_text = this.navParams.get('title_text');
    this.business.business_name = this.navParams.get('business_name');
    this.gstinid = this.navParams.get('gstinid');
    this.userid = this.navParams.get('userid');
    this.buid = this.navParams.get('buid');
    this.action = this.navParams.get('action');
    this.mode = this.navParams.get('mode');

    if(parseInt(this.mode)==2){
      this.gstinid = this.common.selectedBusiness.GSTIN['gstinid'];
      this.userid = this.common.loginData.ID;
      this.business.business_name = this.common.selectedBusiness.business_name;
      this.buid = this.common.selectedBusiness.buid;
      this.business.gstin  = this.common.selectedBusiness.GSTIN['gstin'];
      this.business.branch = this.common.selectedBusiness.GSTIN['branch'];
      this.business.state = this.common.selectedBusiness.GSTIN['statecode'];
      this.business.address_one = this.common.selectedBusiness.GSTIN['address1'];
      this.business.address_two = this.common.selectedBusiness.GSTIN['address2'];
      this.business.place = this.common.selectedBusiness.GSTIN['place'];
      this.business.pincode = this.common.selectedBusiness.GSTIN['zip'];
      this.business.email = this.common.loginData.Email;
      this.business.phone = this.common.selectedBusiness.GSTIN['phoneno'];
      this.selectedState = this.common.selectedBusiness.GSTIN['statecode'];
      this.gstinCode = this.common.selectedBusiness.GSTIN['statecode'];;
      console.log(this.business);
    }
  }

  ionViewDidLoad() {
    this.address = (<HTMLInputElement>document.getElementById("address_one").getElementsByTagName('input')[0]);
    this.options = {
      types: [],
      componentRestrictions: {country: 'in'}
    };

    let componentForm = {
      sublocality_level_1: 'long_name',
      sublocality_level_2: 'long_name',
      locality: 'long_name',
      administrative_area_level_1: 'long_name',
      administrative_area_level_2: 'long_name',
      postal_code: 'long_name'
    };

    let autocomplete = new google.maps.places.Autocomplete(this.address, this.options);
    autocomplete.addListener('place_changed', ()=>{

      let place = autocomplete.getPlace();

      let formatedAddress = (place.formatted_address).split(',');
      this.business.address_one = `${place.name}, ${formatedAddress[0]}, ${formatedAddress[1]}, ${formatedAddress[2]}`;

      for (let i = 0; i < place.address_components.length; i++){
        let addressType = place.address_components[i].types[0];
        if (componentForm[addressType]) {

          let val = place.address_components[i][componentForm[addressType]];

          if(place.address_components[i].types[0]=='sublocality_level_1'){
            this.business.address_two = val;
          }
          if(place.address_components[i].types[0]=='sublocality_level_2'){
            this.business.address_two = val;
          }
          if ((place.address_components[i].types[0] == 'locality') || (place.address_components[i].types[0] == 'administrative_area_level_2')){
            this.business.place = val;
          }
          if(place.address_components[i].types[0]=='postal_code'){
            this.business.pincode = val;
          }

        }
      }
    });
  }

  addressSelected(){
    console.log('Called !');
    let completeAddress = (<HTMLInputElement>document.getElementById('address_one')).value;
    console.log(completeAddress);
  }

  ionViewDidEnter(){
    this.getStates();
  }

  filterState(event){
    const gstinRegxp = /^([0-9]{2}[a-zA-Z]{4}([a-zA-Z]{1}|[0-9]{1})[0-9]{4}[a-zA-Z]{1}([a-zA-Z]|[0-9]){3}){0,15}$/;
    let gstin = event.target.value;
    if(gstin.length>=2){
      let id = gstin.substring(0, 2);
      (this.states).forEach((element)=>{
        if(element.state_code==id){
          this.business.state = element.state_code;
          this.gstinCode = id;
        }
      });
    }
    if((this.business.gstin.length != 15 || !gstinRegxp.test(this.business.gstin))){
      this.invalidGstin = true;
      this.invalidGstinMessage = 'Invalid GSTIN Number';
    }
    else{
      this.invalidGstin = false;
    }
  }

  validateMobile(){
    if((((this.business.phone).toString()).length>10)||(((this.business.phone).toString()).length<10)){
      this.invalidMobile = true;
      this.invalidMobileMessage = 'Invalid Phone Number';
    }
    else{
      this.invalidMobile = false;
    }
  }

  validatePincode(){
    if((((this.business.pincode).toString()).length>6)||(((this.business.pincode).toString()).length<6)){
      this.invalidPincode = true;
      this.invalidPincodeMessage = 'Invalid Pin Code';
    }
    else{
      this.invalidPincode = false;
    }
  }

  save(){

    const emailRegexp = /^[a-zA-Z0-9._%+-]+@[a-zA-Z]+\.[a-zA-Z]{2,4}$/;
    const gstinRegxp = /^([0-9]{2}[a-zA-Z]{4}([a-zA-Z]{1}|[0-9]{1})[0-9]{4}[a-zA-Z]{1}([a-zA-Z]|[0-9]){3}){0,15}$/;

    if(this.business.email == ''){
      this.common.displayToaster('Please fill all fields!');
      return false;
    }
    if ((this.business.gstin.length != 15 || !gstinRegxp.test(this.business.gstin))) {
      this.common.displayToaster('Please enter an valid gstin number!');
      return false;
    }
    if (this.business.email == '') {
      this.common.displayToaster('Please enter your email address!');
      return false;
    }
    if ((this.business.email.length <= 5 || !emailRegexp.test(this.business.email))) {
      this.common.displayToaster('Please enter an valid email address!');
      return false;
    }
    if (this.business.phone == null) {
      this.common.displayToaster('Please enter your mobile number!');
      return false;
    }
    if (isNaN(this.business.phone) || ((((this.business.phone).toString())).length!=10)) {
      this.common.displayToaster('Please enter an valid mobile number!');
      return false;
    }
    if (this.business.pincode == null) {
      this.common.displayToaster('Please enter your pincode!');
      return false;
    }
    if (isNaN(this.business.pincode) || ((((this.business.pincode).toString())).length!=6)) {
      this.common.displayToaster('Please enter an valid pincode!');
      return false;
    }
    if (parseInt(this.gstinCode)!=parseInt(this.business.state)) {
      this.common.displayToaster('Your GSTINID does not match with state name!');
      return false;
    }
    else{
      let message: any;
      if(this.action == 'B'){
        message = 'Business Added Successfully!';
      }
      else if(this.action == 'G'){
        message = 'Branch Added Successfully!';
      }
      else if(this.action == 'U'){
        message = 'Business Updated Successfully!';
      }
      let data = {
        gstinid: this.gstinid,
        userid: parseInt(this.userid),
        business_name: this.business.business_name,
        buid: parseInt(this.buid),
        gstin: this.business.gstin,
        branch: this.business.branch,
        statecode: parseInt(this.business.state),
        address1: this.business.address_one,
        address2: this.business.address_two,
        place: this.business.place,
        state: this.getStateName(this.business.state),
        zip: this.business.pincode,
        action: this.action,
        email: this.business.email,
        phoneno: this.business.phone
      };
      console.log(data);
      this.common.displayLoader('Please wait...');
      this.api.post('addbusiness', data).subscribe((res)=>{
        console.log(res);
        this.common.hideLoader();
        if(res['status']==false)
        {
          this.common.displayToaster(res['message']);
        }
        else
        {
          this.common.displayToaster(message);
          this.viewCtrl.dismiss(res);
        }
      }, (err)=>{
        console.log(err);
        this.common.displayToaster('Unable to add business!');
        this.common.hideLoader();
      });
    }
  }

  cancel(){
    let data = {role:'cancel'};
    this.viewCtrl.dismiss(data);
  }

  getStates(){
    this.api.get('getstate').subscribe((res)=>{
      this.states = JSON.parse(atob(res['stateList']));
      console.log(this.states);
      if(this.selectedState!=null){
        (this.states).forEach(element => {
          if(parseInt(element.state_code) == parseInt(this.selectedState)){
            this.business.state = element.state_code;
          }
        });
      }
    }, (err)=>{
      console.log(err);
      this.common.displayToaster('Oops, Something went wrong!');
    });
  }

  getStateName(id: any){
    let name:any;
    (this.states).forEach(element => {
      if(parseInt(element.state_code) == parseInt(id)){
        name = element.state_name;
      }
    });
    return name;
  }
}
