import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Nav, AlertController } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { TranslateService } from './../../providers/translate';
import { CommonService } from './../../providers/common';

@IonicPage()
@Component({
  selector: 'page-sidemenu',
  templateUrl: 'sidemenu.html',
})
export class SidemenuPage {

  @ViewChild(Nav) nav: Nav;
  rootPage: any = 'DashboardPage';

  constructor(public translate: TranslateService, public iab: InAppBrowser, public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public common: CommonService) {}

  openItemsPage(){
    this.navCtrl.push('ItemsPage');
  }

  openClientsPage(){
    this.navCtrl.push('ClientsPage');
  }

  openScanItemPage(){
    this.navCtrl.push('ScanItemPage');
  }

  openShareFilePagePage(){
    this.navCtrl.push('ShareFilePage');
  }

  openSettingsPage(){
    this.navCtrl.push('SettingsPage');
  }

  openContactusPage(){
    this.navCtrl.push('ContactusPage');
  }

  openYouTube(){
    this.iab.create('https://www.youtube.com/channel/UCEK5QeRfpvkGkZHuIdqr5WQ', '_blank', 'location=no');
  }

  openPlayStore(){
    this.iab.create('https://play.google.com/store/apps/details?id=hostbooks.com.eway.bill', '_blank', 'location=no');
  }

  changeBusiness(){
    localStorage.removeItem('selectedBusiness');
    localStorage.removeItem('businessList');
    localStorage.removeItem('itemList');
    localStorage.removeItem('clientList');
    localStorage.removeItem('billDetails');
    localStorage.removeItem('AuthToken');
    this.navCtrl.setRoot('HomePage');
  }

  changeLanguage(){
    let alert = this.alertCtrl.create();
    alert.setTitle('Select Language');

    (this.translate.languages).forEach((element)=>{
      if(element.code == this.translate.selectedlanguage){
        alert.addInput({
          type: 'radio',
          label: element.name,
          value: element.code,
          checked: true
        });
      }
      else{
        alert.addInput({
          type: 'radio',
          label: element.name,
          value: element.code
        });
      }
    });

    alert.addButton({
      text: 'Cancel',
      cssClass: 'cancelAlertButton',
      role: 'cancel',
      handler: () => {
        this.common.displayToaster('Language Changed Cancelled!');
      }
    });
    alert.addButton({
      text: 'Select',
      cssClass: 'submitAlertButton',
      role: 'submit',
      handler: data => {
        localStorage.setItem('language', data);
        this.translate.selectedlanguage = data;
        this.common.displayToaster('Language Changed Successfully!');
      }
    });
    alert.present();
  }

  logout(){
    const prompt = this.alertCtrl.create({
      title: 'Confirm',
      message: "Are you sure you want to logout?",
      buttons: [
        {
          text: 'No',
          cssClass: 'cancelAlertButton',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          cssClass: 'submitAlertButton',
          handler: data => {
            localStorage.clear();
            this.common.displayToaster('Logged Out Successfully!');
            localStorage.setItem('language', this.translate.selectedlanguage);
            this.navCtrl.setRoot('LoginPage');
          }
        }
      ]
    });
    prompt.present();
  }

}
