export interface User{
  name?: string,
  mobile?: number,
	email?: string,
	password?: string,
  confirm_password?: string,
  who?:string
}
