import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { TranslateService } from './../../providers/translate';
import { CommonService } from './../../providers/common';
import { ApiService } from './../../providers/api';
import { BillTo } from '../../modal/bill-to-modal';

@IonicPage()
@Component({
  selector: 'page-add-bill-to',
  templateUrl: 'add-bill-to.html',
})
export class AddBillToPage {

  billTo: BillTo = {};
  header_text: string = '';
  states: any = [];
  clientList: any = [];
  tempClientList: any = [];

  displayClients: boolean = false;

  mode: number = 1;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public translate: TranslateService, public common: CommonService, public api: ApiService) {
    this.header_text = this.navParams.get('header_text');
    this.mode = this.navParams.get('mode');

    if(this.common.selectedBillTo!=null){
      let data = this.common.selectedBillTo;
      this.billTo.bill_to_name = data.business_name;
      this.billTo.bill_to_gstin = data.GSTIN['gstin'];
      this.billTo.bill_to_state = data.GSTIN['statecode'];
      this.billTo.bill_to_state_code = data.GSTIN['statecode'];
      this.billTo.bill_to_address_one = data.GSTIN['address1'];
      this.billTo.bill_to_address_two = data.GSTIN['address2'];
      this.billTo.bill_to_place = data.GSTIN['place'];
      this.billTo.bill_to_pincode = data.GSTIN['zip'];
      console.log(data);
    }

    if(this.mode==2){
      let data = JSON.parse(localStorage.getItem('billDetails'));
      this.billTo.bill_to_name = data.toTrdName;
      this.billTo.bill_to_gstin = data.toGstin;
      this.billTo.bill_to_state = data.toStateCode;
      this.billTo.bill_to_state_code = data.toSupply;
      this.billTo.bill_to_address_one = data.toAddr1;
      this.billTo.bill_to_address_two = data.toAddr2;
      this.billTo.bill_to_place = data.toPlace;
      this.billTo.bill_to_pincode = data.toPincode;
      console.log(data);
    }
    this.clientList = JSON.parse(localStorage.getItem('clientList'));
    this.tempClientList = JSON.parse(localStorage.getItem('clientList'));
    this.getStates();
  }

  save(){
    const gstinRegxp = /^([0-9]{2}[a-zA-Z]{4}([a-zA-Z]{1}|[0-9]{1})[0-9]{4}[a-zA-Z]{1}([a-zA-Z]|[0-9]){3}){0,15}$/;

    if((this.billTo.bill_to_gstin).toUpperCase()!='URP'){
      if ((this.billTo.bill_to_gstin.length != 15) || (!gstinRegxp.test(this.billTo.bill_to_gstin))) {
        this.common.displayToaster('Please enter a valid gstin number!');
        return false;
      }
    }
    if (((this.billTo.bill_to_pincode).toString()).length!=6) {
      this.common.displayToaster('Please enter a valid pincode!');
      return false;
    }
    else{
      let data = this.billTo;
      this.viewCtrl.dismiss(data);
    }
  }

  cancel(){
    let data = { };
    this.viewCtrl.dismiss(data);
  }

  getStates(){
    this.api.get('getstate').subscribe((res)=>{
      this.states = JSON.parse(atob(res['stateList']));
      console.log(this.states);
      (this.states).forEach((element)=>{
        if(parseInt(element.state_code)==parseInt(this.billTo.bill_to_state)){
         this.billTo.bill_to_state = element.state_code;
         this.billTo.bill_to_state_code = element.state_code;
        }
      });
    }, (err)=>{
      console.log(err);
      this.common.displayToaster('Oops, Something went wrong!');
    });
  }

  searchClient(event){
    this.displayClients = true;
    this.initializeClients();
    const val = event.target.value;
    if (val && val.trim() != '') {
      this.clientList = this.clientList.filter((client) => {
        return (((client.clientname).toLowerCase()).indexOf(val.toLowerCase()) > -1);
      });
    }
    if((this.clientList).length==0){
      this.clientList.push({clientname:'No Record Found!', state_name:'', clientid: '0'});
    }
  }

  initializeClients(){
    this.clientList = this.tempClientList;
  }

  selectClient(value){
    this.displayClients = false;
    let  userid = btoa(this.common.loginData.ID);
    let  buid = btoa(this.common.selectedBusiness.buid);
    let  gstinid = btoa(this.common.selectedBusiness.GSTIN.gstinid);
    let  clientid = parseInt(value.clientid);
    let  isaction = 'V';

    this.api.get(`getclient/${userid}/${buid}/${gstinid}/${clientid}/${isaction}/`).subscribe((res)=>{
      if(res['status']==false){
      }
      else{
        let data = JSON.parse(atob(res['client']))[0];
        this.billTo.bill_to_name = data.clientname;
        this.billTo.bill_to_gstin = data.gstin;
        this.billTo.bill_to_state = data.state_id;
        this.billTo.bill_to_state_code = data.state_id;
        this.billTo.bill_to_address_one = data.address1;
        this.billTo.bill_to_address_two = data.address2;
        this.billTo.bill_to_place = data.city;
        this.billTo.bill_to_pincode = data.pincode;
      }
    }, (err)=>{
      console.log(err);
      this.common.displayToaster('Oops, Something went wrong!');
    });
  }
}
