export interface Item{
  item_id?: number,
  item_description?: string,
  item_type?: string,
  item_hsn_sac_code?: number,
  item_quantity?:number,
  item_sku_code?: string,
  item_tax_rate?: number,
  item_igst?:number,
  item_cgst?:number,
  item_sgst?:number,
  item_cess_rate?: number,
  item_cess_amount?: number,
  item_purchase_price?: number,
  item_selling_price?: number,
  item_unit?: string,
  item_note?: string,
  item_total?: number
}
