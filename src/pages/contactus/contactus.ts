import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';
import { EmailComposer } from '@ionic-native/email-composer';
import { TranslateService } from './../../providers/translate';
import { CommonService } from './../../providers/common';
import { ApiService } from './../../providers/api';

@IonicPage()
@Component({
  selector: 'page-contactus',
  templateUrl: 'contactus.html',
})
export class ContactusPage {

  message: string = '';

  constructor(public navCtrl: NavController, public navParams: NavParams , public platform: Platform, public callNumber: CallNumber, public emailComposer: EmailComposer, public translate: TranslateService, public api: ApiService, public common: CommonService) {
  }

  sendFeedback(){
    if(this.message==''){
      this.common.displayToaster('Please enter your message');
      return false;
    }
    else{
      let email = {
        to: 'info@hostbooks.com',
        subject: 'Customer Contact Form',
        body: this.message,
        isHtml: true
      };
      this.emailComposer.open(email);
      this.common.displayToaster('Composer launched successfully!');
    }
  }

  callme(){
    this.callNumber.callNumber("+919152282828", true).then((res) => {
      console.log('Launched dialer!', res);
      this.common.displayToaster('Dialer launched successfully!');
    }).catch((err) => {
      console.log('Error launching dialer', err);
      this.common.displayToaster('Oops, Something went wrong!');
    });
  }

}
