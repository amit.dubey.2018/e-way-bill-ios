import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  AlertController
} from "ionic-angular";
import { TranslateService } from "./../../providers/translate";
import { CommonService } from "./../../providers/common";
import { ApiService } from "./../../providers/api";

@IonicPage()
@Component({
  selector: "page-dashboard",
  templateUrl: "dashboard.html"
})
export class DashboardPage {
  firstActive: boolean = true;
  secondActive: boolean = false;
  thirdActive: boolean = false;
  fourthActive: boolean = false;
  ewbInput: boolean = true;
  ewbGenerated: boolean = false;
  displayAddBillButton: boolean = true;

  bills: any = [];
  inputBillsList: any = [];
  tempInputBillsList: any = [];
  generatedBillsList: any = [];
  tempGeneratedBillsList: any = [];
  items: any = [];
  tempItems: any = [];
  clients: any = [];
  tempClients: any = [];
  gstinRegxp = /^([0-9]{2}[a-zA-Z]{4}([a-zA-Z]{1}|[0-9]{1})[0-9]{4}[a-zA-Z]{1}([a-zA-Z]|[0-9]){3}){0,15}$/;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public translate: TranslateService,
    public api: ApiService,
    public common: CommonService
  ) {}

  ionViewDidEnter() {
    this.inputBillList();
  }

  inputBillList() {
    this.firstActive = true;
    this.secondActive = false;
    this.thirdActive = false;

    this.ewbInput = true;
    this.ewbGenerated = false;
    this.displayAddBillButton = true;

    let userid = btoa(this.common.loginData.ID);
    let buid = btoa(this.common.selectedBusiness.buid);
    let gstinid = btoa(this.common.selectedBusiness.GSTIN.gstinid);
    let isaction = "N";
    this.common.displayLoader("Please wait...");
    this.api
      .get(`get-ewb-list/${userid}/${buid}/${gstinid}/${isaction}`)
      .subscribe(
        res => {
          this.common.hideLoader();
          if (res["status"] == false) {
            this.common.displayToaster(res["message"]);
          } else {
            this.inputBillsList = JSON.parse(atob(res["ewbList"]));
            this.tempInputBillsList = JSON.parse(atob(res["ewbList"]));
            console.log(this.inputBillsList);
          }
        },
        err => {
          console.log(err);
          this.common.displayToaster("Oops, Something went wrong!");
          this.common.hideLoader();
        }
      );
  }

  generatedBillList() {
    this.firstActive = true;
    this.secondActive = false;
    this.thirdActive = false;

    this.ewbInput = false;
    this.ewbGenerated = true;

    let userid = btoa(this.common.loginData.ID);
    let buid = btoa(this.common.selectedBusiness.buid);
    let gstinid = btoa(this.common.selectedBusiness.GSTIN.gstinid);
    let isaction = "Y";
    this.common.displayLoader("Please wait...");
    this.api
      .get(`get-ewb-list/${userid}/${buid}/${gstinid}/${isaction}`)
      .subscribe(
        res => {
          this.common.hideLoader();
          if (res["status"] == false) {
            this.common.displayToaster(res["message"]);
          } else {
            this.generatedBillsList = JSON.parse(atob(res["ewbList"]));
            this.tempGeneratedBillsList = JSON.parse(atob(res["ewbList"]));
            console.log(this.generatedBillsList);
          }
        },
        err => {
          console.log(err);
          this.common.displayToaster("Oops, Something went wrong!");
          this.common.hideLoader();
        }
      );
  }

  searchBills(event) {
    this.initializeBills();
    const val = event.target.value;
    if (this.ewbGenerated) {
      if (val && val.trim() != "") {
        this.generatedBillsList = this.generatedBillsList.filter(bill => {
          return (
            bill.fromTrdName.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
            bill.toTrdName.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
            bill.supplyTypeDesc.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
            bill.docNo.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
            bill.ewayBillNo.toLowerCase().indexOf(val.toLowerCase()) > -1
          );
        });
      }
    } else {
      if (val && val.trim() != "") {
        this.inputBillsList = this.inputBillsList.filter(bill => {
          return (
            bill.fromTrdName.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
            bill.toTrdName.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
            bill.supplyTypeDesc.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
            bill.docNo.toLowerCase().indexOf(val.toLowerCase()) > -1
          );
        });
      }
    }
  }

  initializeBills() {
    if (this.ewbGenerated) {
      this.generatedBillsList = this.tempGeneratedBillsList;
    } else {
      this.inputBillsList = this.tempInputBillsList;
    }
  }

  itemsList() {
    this.firstActive = false;
    this.secondActive = true;
    this.thirdActive = false;
    this.displayAddBillButton = false;

    let userid = btoa(this.common.loginData.ID);
    let buid = btoa(this.common.selectedBusiness.buid);
    let gstinid = btoa(this.common.selectedBusiness.GSTIN.gstinid);
    let itemid = 0;
    let isaction = "L";
    this.common.displayLoader("Please wait...");
    this.api
      .get(`getitemmster/${userid}/${buid}/${gstinid}/${itemid}/${isaction}`)
      .subscribe(
        res => {
          console.log(res);
          if (res["status"] == false) {
            this.items = [];
            this.common.displayToaster(res["message"]);
          } else {
            this.items = JSON.parse(atob(res["itemList"]));
            this.tempItems = JSON.parse(atob(res["itemList"]));
            localStorage.setItem("itemList", atob(res["itemList"]));
          }
          console.log(this.items);
          this.common.hideLoader();
        },
        err => {
          console.log(err);
          this.common.displayToaster("Oops, Something went wrong!");
          this.common.hideLoader();
        }
      );
  }

  searchItem(event) {
    this.initializeItems();
    const val = event.target.value;
    if (val && val.trim() != "") {
      this.items = this.items.filter(item => {
        console.log(item);
        return (
          item.item_description.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
          item.hsn_sac_code.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
          item.item_sku_code.toLowerCase().indexOf(val.toLowerCase()) > -1
        );
      });
    }
  }

  initializeItems() {
    this.items = this.tempItems;
  }

  clientsList() {
    this.firstActive = false;
    this.secondActive = false;
    this.thirdActive = true;
    this.displayAddBillButton = false;

    let userid = btoa(this.common.loginData.ID);
    let buid = btoa(this.common.selectedBusiness.buid);
    let gstinid = btoa(this.common.selectedBusiness.GSTIN.gstinid);
    let clientid = 0;
    let isaction = "L";
    this.common.displayLoader("Please wait...");
    this.api
      .get(`getclient/${userid}/${buid}/${gstinid}/${clientid}/${isaction}/`)
      .subscribe(
        res => {
          if (res["status"] == false) {
            this.clients = [];
            this.common.displayToaster(res["message"]);
          } else {
            this.clients = JSON.parse(atob(res["client"]));
            this.tempClients = JSON.parse(atob(res["client"]));
            localStorage.setItem("clientList", atob(res["client"]));
          }
          console.log(this.clients);
          this.common.hideLoader();
        },
        err => {
          console.log(err);
          this.common.displayToaster("Oops, Something went wrong!");
          this.common.hideLoader();
        }
      );
  }

  searchClient(event) {
    this.initializeClients();
    const val = event.target.value;
    if (val && val.trim() != "") {
      this.clients = this.clients.filter(client => {
        return (
          client.clientname.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
          client.state_name.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
          client.gstin.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
          client.pan.toLowerCase().indexOf(val.toLowerCase()) > -1
        );
      });
    }
  }

  initializeClients() {
    this.clients = this.tempClients;
  }

  addEwayBill() {
    let items = JSON.parse(localStorage.getItem("itemList"));
    let clients = JSON.parse(localStorage.getItem("clientList"));
    if (items.length == 0) {
      this.common.displayToaster(`You haven't added any item yet!`);
      let data = {
        button_text: "Add Item",
        itemData: null
      };
      let addItemModal = this.modalCtrl.create("ItemDetailsPage", data);
      addItemModal.onDidDismiss(data => {
        this.itemsList();
      });
      addItemModal.present();
    } else if (clients.length == 0) {
      this.common.displayToaster(`You haven't added any client yet!`);
      let data = {
        button_text: "Add Client",
        clientData: null
      };
      let addCustomerModal = this.modalCtrl.create("ClientDetailsPage", data);
      addCustomerModal.onDidDismiss(data => {
        console.log(data);
        this.clientsList();
      });
      addCustomerModal.present();
    } else {
      this.navCtrl.push("AddBillPage");
    }
  }

  generateEwayBill(value) {
    let authToken = JSON.parse(localStorage.getItem("AuthToken"));
    let tokenSaveTime = JSON.parse(localStorage.getItem("TokenSaveTime"));
    let savedTime = new Date(tokenSaveTime);
    let currentTime = new Date();
    let timeDiff = Math.round(
      (currentTime.getTime() - savedTime.getTime()) / 1000 / 60
    );
    console.log(savedTime, currentTime);
    console.log(timeDiff);

    if (authToken != null && timeDiff <= 360) {
      let billdata = {
        userid: this.common.loginData.ID,
        buid: this.common.selectedBusiness.buid,
        gstinid: this.common.selectedBusiness.GSTIN.gstinid,
        inv_no: value.inv_no,
        sek: authToken["sek"],
        app_token: authToken["app_token"],
        asp_secret_key: authToken["asp_secret_key"],
        session_id: authToken["session_id"],
        usergstin: this.common.selectedBusiness.GSTIN.gstin,
        authtoken: authToken["authtoken"]
      };
      console.log(billdata);
      this.common.displayLoader("Please wait...");
      this.api.post("generate-ewb-bill", billdata).subscribe(
        res => {
          console.log(res);
          this.common.hideLoader();
          if (res["status"] == true) {
            this.common.displayToaster("Bill Generated Successfully!");
            const prompt = this.alertCtrl.create({
              title: "Generated!",
              message: `Your e-way bill no is ${res['ewayBillNo']}.`,
              buttons: [
                {
                  text: "GOT IT!",
                  cssClass: "submitAlertButton",
                  handler: data => {
                    this.generatedBillList();
                  }
                }
              ]
            });
            prompt.present();
          } else {
            this.common.displayToaster(res["message"]);
          }
        },
        err => {
          console.log(err);
          this.common.displayToaster("Oops, Something went wrong!");
          this.common.hideLoader();
        }
      );
    } else {
      this.generateEwayBillViaLogin(value);
    }
  }

  generateEwayBillViaLogin(value) {
    console.log(value);
    const prompt = this.alertCtrl.create({
      title: "NSDL Login Credentials",
      inputs: [
        {
          name: "username",
          type: "text",
          placeholder: "Username/GSTIN"
        },
        {
          name: "password",
          type: "password",
          placeholder: "Password"
        }
      ],
      buttons: [
        {
          text: "Cancel",
          cssClass: "cancelAlertButton",
          handler: data => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Login",
          cssClass: "submitAlertButton",
          handler: data => {
            if (!data.username) {
              this.common.displayToaster("Username is required!");
              return false;
            }
            if (!data.password) {
              this.common.displayToaster("Password is required!");
              return false;
            } else {
              let userdata = {
                usergstin: this.common.selectedBusiness.GSTIN.gstin,
                username: data.username,
                password: data.password
              };
              console.log(userdata);
              this.common.displayLoader("Please wait...");
              this.api.post("generate-token", userdata).subscribe(
                res => {
                  console.log(res);
                  if (res["status"] == false) {
                    this.common.displayToaster(res["message"]);
                    this.common.hideLoader();
                  } else {
                    localStorage.setItem(
                      "AuthToken",
                      JSON.stringify(res["token"])
                    );
                    let date = new Date();
                    localStorage.setItem("TokenSaveTime", JSON.stringify(date));
                    let authToken = res["token"];
                    this.common.displayToaster(
                      "Auth Token Generated Successfully!"
                    );
                    this.common.hideLoader();
                    const prompt = this.alertCtrl.create({
                      title: "Login Success!",
                      message: "Generate E-Way Bill",
                      buttons: [
                        {
                          text: "Cancel",
                          cssClass: "cancelAlertButton",
                          handler: data => {
                            this.common.displayToaster(
                              "Bill Generation Cancelled!"
                            );
                          }
                        },
                        {
                          text: "Generate!",
                          cssClass: "submitAlertButton",
                          handler: data => {
                            let billdata = {
                              userid: this.common.loginData.ID,
                              buid: this.common.selectedBusiness.buid,
                              gstinid: this.common.selectedBusiness.GSTIN
                                .gstinid,
                              inv_no: value.inv_no,
                              sek: authToken["sek"],
                              app_token: authToken["app_token"],
                              asp_secret_key: authToken["asp_secret_key"],
                              session_id: authToken["session_id"],
                              usergstin: userdata.usergstin,
                              authtoken: authToken["authtoken"]
                            };
                            console.log(billdata);
                            this.common.displayLoader("Please wait...");
                            this.api
                              .post("generate-ewb-bill", billdata)
                              .subscribe(
                                res => {
                                  console.log(res);
                                  this.common.hideLoader();
                                  if (res["status"] == true) {
                                    this.common.displayToaster(
                                      "Bill Generated Successfully!"
                                    );
                                    const prompt = this.alertCtrl.create({
                                      title: "Generated!",
                                      message: `Your e-way bill no is ${res['ewayBillNo']}.`,
                                      buttons: [
                                        {
                                          text: "GOT IT!",
                                          cssClass: "submitAlertButton",
                                          handler: data => {
                                            this.generatedBillList();
                                          }
                                        }
                                      ]
                                    });
                                    prompt.present();
                                  } else {
                                    this.common.displayToaster(res["message"]);
                                  }
                                },
                                err => {
                                  console.log(err);
                                  this.common.displayToaster(
                                    "Oops, Something went wrong!"
                                  );
                                  this.common.hideLoader();
                                }
                              );
                          }
                        }
                      ]
                    });
                    prompt.present();
                  }
                },
                err => {
                  console.log(err);
                  this.common.displayToaster("Oops, Something went wrong!");
                  this.common.hideLoader();
                }
              );
            }
          }
        }
      ]
    });
    prompt.present();
  }

  cancelEwayBill(billdata) {
    console.log(billdata);
    let authToken = JSON.parse(localStorage.getItem("AuthToken"));
    let tokenSaveTime = JSON.parse(localStorage.getItem("TokenSaveTime"));
    let savedTime = new Date(tokenSaveTime);
    let currentTime = new Date();
    let timeDiff = Math.round(
      (currentTime.getTime() - savedTime.getTime()) / 1000 / 60
    );
    console.log(savedTime, currentTime);
    console.log(timeDiff);

    const prompt = this.alertCtrl.create({
      title: "Cancel Bill",
      message: "Are you sure to cancel?",
      buttons: [
        {
          text: "No",
          cssClass: "cancelAlertButton",
          handler: data => {}
        },
        {
          text: "Yes",
          cssClass: "submitAlertButton",
          handler: data => {
            if (authToken != null && timeDiff <= 360) {
              const prompt = this.alertCtrl.create({
                title: "Reason For Cancellation",
                inputs: [
                  {
                    type: "radio",
                    label: "Duplicate",
                    value: "1",
                    name: "reasonCode"
                  },
                  {
                    type: "radio",
                    label: "Order Cancelled",
                    value: "2",
                    name: "reasonCode"
                  },
                  {
                    type: "radio",
                    label: "Data Entry mistake",
                    value: "3",
                    name: "reasonCode"
                  },
                  {
                    type: "radio",
                    label: "Others",
                    value: "4",
                    name: "reasonCode"
                  }
                ],
                buttons: [
                  {
                    text: "Cancel",
                    cssClass: "cancelAlertButton",
                    handler: data => {
                      this.common.displayToaster(
                        "Bill Cancellation Cancelled!"
                      );
                    }
                  },
                  {
                    text: "OK",
                    cssClass: "submitAlertButton",
                    handler: data => {
                      let reasonCode = data;
                      const prompt = this.alertCtrl.create({
                        title: "Reason Description",
                        inputs: [
                          {
                            type: "text",
                            placeholder: "Description",
                            name: "reason"
                          }
                        ],
                        buttons: [
                          {
                            text: "Cancel",
                            cssClass: "cancelAlertButton",
                            handler: data => {
                              this.common.displayToaster(
                                "Bill Cancellation Cancelled!"
                              );
                            }
                          },
                          {
                            text: "OK",
                            cssClass: "submitAlertButton",
                            handler: data => {
                              let reason = data.reason;
                              console.log(reasonCode, reason);
                              let canceldata = {
                                userid: this.common.loginData.ID,
                                buid: this.common.selectedBusiness.buid,
                                gstinid: this.common.selectedBusiness.GSTIN
                                  .gstinid,
                                invno: billdata.inv_no,
                                ewbNo: billdata.ewayBillNo,
                                cancelRsnCode: reasonCode,
                                cancelRmrk: reason,
                                sek: authToken["sek"],
                                app_token: authToken["app_token"],
                                asp_secret_key: authToken["asp_secret_key"],
                                session_id: authToken["session_id"],
                                usergstin: this.common.selectedBusiness.GSTIN
                                  .gstin,
                                authtoken: authToken["authtoken"]
                              };
                              console.log(canceldata);
                              this.common.displayLoader("Please wait...");
                              this.api.post("cancel-ewb", canceldata).subscribe(
                                res => {
                                  console.log(res);
                                  this.common.hideLoader();
                                  if (res["status"] == true) {
                                    this.common.displayToaster(
                                      "Bill Cancelled Successfully!"
                                    );
                                    const prompt = this.alertCtrl.create({
                                      title: "Cancelled",
                                      message: `Your e-way bill no. ${billdata.ewayBillNo} is cancelled successfully!`,
                                      buttons: [
                                        {
                                          text: "GOT IT!",
                                          cssClass: "submitAlertButton",
                                          handler: data => {
                                            this.generatedBillList();
                                          }
                                        }
                                      ]
                                    });
                                    prompt.present();
                                  } else {
                                    this.common.displayToaster(res["message"]);
                                  }
                                },
                                err => {
                                  console.log(err);
                                  this.common.displayToaster(
                                    "Oops, Something went wrong!"
                                  );
                                  this.common.hideLoader();
                                }
                              );
                            }
                          }
                        ]
                      });
                      prompt.present();
                    }
                  }
                ]
              });
              prompt.present();
            } else {
              this.cancelEwayBillViaLogin(billdata);
            }
          }
        }
      ]
    });
    prompt.present();
  }

  cancelEwayBillViaLogin(billdata) {
    console.log(billdata);
    const prompt = this.alertCtrl.create({
      title: "NSDL Login Credentials",
      inputs: [
        {
          name: "username",
          type: "text",
          placeholder: "Username/GSTIN"
        },
        {
          name: "password",
          type: "password",
          placeholder: "Password"
        }
      ],
      buttons: [
        {
          text: "Cancel",
          cssClass: "cancelAlertButton",
          handler: data => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Login",
          cssClass: "submitAlertButton",
          handler: data => {
            if (!data.username) {
              this.common.displayToaster("Username is required!");
              return false;
            }
            if (!data.password) {
              this.common.displayToaster("Password is required!");
              return false;
            } else {
              let userdata = {
                usergstin: this.common.selectedBusiness.GSTIN.gstin,
                username: data.username,
                password: data.password
              };
              console.log(userdata);
              this.common.displayLoader("Please wait...");
              this.api.post("generate-token", userdata).subscribe(
                res => {
                  console.log(res);
                  if (res["status"] == false) {
                    this.common.displayToaster(res["message"]);
                    this.common.hideLoader();
                  } else {
                    localStorage.setItem(
                      "AuthToken",
                      JSON.stringify(res["token"])
                    );
                    let date = new Date();
                    localStorage.setItem("TokenSaveTime", JSON.stringify(date));
                    let authToken = res["token"];
                    this.common.displayToaster(
                      "Auth Token Generated Successfully!"
                    );
                    this.common.hideLoader();
                    const prompt = this.alertCtrl.create({
                      title: "Reason For Cancellation",
                      inputs: [
                        {
                          type: "radio",
                          label: "Duplicate",
                          value: "1",
                          name: "reasonCode"
                        },
                        {
                          type: "radio",
                          label: "Order Cancelled",
                          value: "2",
                          name: "reasonCode"
                        },
                        {
                          type: "radio",
                          label: "Data Entry mistake",
                          value: "3",
                          name: "reasonCode"
                        },
                        {
                          type: "radio",
                          label: "Others",
                          value: "4",
                          name: "reasonCode"
                        }
                      ],
                      buttons: [
                        {
                          text: "Cancel",
                          cssClass: "cancelAlertButton",
                          handler: data => {
                            this.common.displayToaster(
                              "Bill Cancellation Cancelled!"
                            );
                          }
                        },
                        {
                          text: "OK",
                          cssClass: "submitAlertButton",
                          handler: data => {
                            let reasonCode = data;
                            const prompt = this.alertCtrl.create({
                              title: "Reason Description",
                              inputs: [
                                {
                                  type: "text",
                                  placeholder: "Description",
                                  name: "reason"
                                }
                              ],
                              buttons: [
                                {
                                  text: "Cancel",
                                  cssClass: "cancelAlertButton",
                                  handler: data => {
                                    this.common.displayToaster(
                                      "Bill Cancellation Cancelled!"
                                    );
                                  }
                                },
                                {
                                  text: "OK",
                                  cssClass: "submitAlertButton",
                                  handler: data => {
                                    let reason = data.reason;
                                    console.log(reasonCode, reason);
                                    let canceldata = {
                                      userid: this.common.loginData.ID,
                                      buid: this.common.selectedBusiness.buid,
                                      gstinid: this.common.selectedBusiness.GSTIN
                                        .gstinid,
                                      invno: billdata.inv_no,
                                      ewbNo: billdata.ewayBillNo,
                                      cancelRsnCode: reasonCode,
                                      cancelRmrk: reason,
                                      sek: authToken["sek"],
                                      app_token: authToken["app_token"],
                                      asp_secret_key: authToken["asp_secret_key"],
                                      session_id: authToken["session_id"],
                                      usergstin: this.common.selectedBusiness.GSTIN
                                        .gstin,
                                      authtoken: authToken["authtoken"]
                                    };
                                    console.log(canceldata);
                                    this.common.displayLoader("Please wait...");
                                    this.api.post("cancel-ewb", canceldata).subscribe(
                                      res => {
                                        console.log(res);
                                        this.common.hideLoader();
                                        if (res["status"] == true) {
                                          this.common.displayToaster(
                                            "Bill Cancelled Successfully!"
                                          );
                                          const prompt = this.alertCtrl.create({
                                            title: "Cancelled",
                                            message: `Your e-way bill no. ${billdata.ewayBillNo} is cancelled successfully!`,
                                            buttons: [
                                              {
                                                text: "GOT IT!",
                                                cssClass: "submitAlertButton",
                                                handler: data => {
                                                  this.generatedBillList();
                                                }
                                              }
                                            ]
                                          });
                                          prompt.present();
                                        } else {
                                          this.common.displayToaster(res["message"]);
                                        }
                                      },
                                      err => {
                                        console.log(err);
                                        this.common.displayToaster(
                                          "Oops, Something went wrong!"
                                        );
                                        this.common.hideLoader();
                                      }
                                    );
                                  }
                                }
                              ]
                            });
                            prompt.present();
                          }
                        }
                      ]
                    });
                    prompt.present();
                  }
                },
                err => {
                  console.log(err);
                  this.common.displayToaster("Oops, Something went wrong!");
                  this.common.hideLoader();
                }
              );
            }
          }
        }
      ]
    });
    prompt.present();
  }

  updateVehicleDetails(vehicleData) {
    console.log(vehicleData);
    let data = {
      header_text: "Update Vehicle"
    };
    let updateVehicleModal = this.modalCtrl.create("VehicleUpdatePage", data);
    updateVehicleModal.onDidDismiss(updatedVehicleData => {
      console.log(updatedVehicleData["fromState"]);
      if (updatedVehicleData["fromState"] != undefined) {
        let reasonRem: any;
        if (updatedVehicleData.reasonCode == 1) {
          reasonRem = "Due to Break Down";
        } else if (updatedVehicleData.reasonCode == 2) {
          reasonRem = "Due to Transshipment";
        } else if (updatedVehicleData.reasonCode == 3) {
          reasonRem = "Other";
        } else if (updatedVehicleData.reasonCode == 4) {
          reasonRem = "First Time";
        }
        updatedVehicleData.ewbNo = vehicleData.ewayBillNo;
        updatedVehicleData.reasonRem = reasonRem;
        updatedVehicleData.transDocDate = this.common.formatTransportDate(updatedVehicleData.transDocDate);

        let authToken = JSON.parse(localStorage.getItem("AuthToken"));
        let tokenSaveTime = JSON.parse(localStorage.getItem("TokenSaveTime"));
        let savedTime = new Date(tokenSaveTime);
        let currentTime = new Date();
        let timeDiff = Math.round(
          (currentTime.getTime() - savedTime.getTime()) / 1000 / 60
        );
        console.log(savedTime, currentTime);
        console.log(timeDiff);

        if (authToken != null && timeDiff <= 360) {
          const prompt = this.alertCtrl.create({
            title: "Login Success",
            message: "Proceed for vehicle update!",
            buttons: [
              {
                text: "Cancel",
                cssClass: "cancelAlertButton",
                handler: () => {
                  this.common.displayToaster("Vehicle Update Cancelled!");
                }
              },
              {
                text: "Update",
                cssClass: "submitAlertButton",
                handler: data => {
                  let updateData = {
                    userid: this.common.loginData.ID,
                    buid: this.common.selectedBusiness.buid,
                    gstinid: this.common.selectedBusiness.GSTIN.gstinid,
                    inv_no: vehicleData.inv_no,
                    sek: authToken["sek"],
                    app_token: authToken["app_token"],
                    asp_secret_key: authToken["asp_secret_key"],
                    session_id: authToken["session_id"],
                    usergstin: this.common.selectedBusiness.GSTIN.gstin,
                    authtoken: authToken["authtoken"],
                    vehicles: updatedVehicleData
                  };

                  console.log(updateData);

                  this.common.displayLoader("Please wait...");
                  this.api.post("update-vehicle-ewb", updateData).subscribe(
                    res => {
                      console.log(res);
                      this.common.hideLoader();
                      if (res["status"] == true) {
                        this.common.displayToaster(
                          "Vehicle Details Updated Successfully!"
                        );
                        const prompt = this.alertCtrl.create({
                          title: "Updated",
                          message: `Your vehicle details updated successfully!`,
                          buttons: [
                            {
                              text: "GOT IT!",
                              cssClass: "submitAlertButton",
                              handler: data => {
                                this.generatedBillList();
                              }
                            }
                          ]
                        });
                        prompt.present();
                      } else {
                        this.common.displayToaster(res["message"]);
                      }
                    },
                    err => {
                      console.log(err);
                      this.common.displayToaster("Oops, Something went wrong!");
                      this.common.hideLoader();
                    }
                  );
                }
              }
            ]
          });
          prompt.present();
        }
        else
        {
          this.updateVehicleDetailsViaLogin(vehicleData, updatedVehicleData);
        }
      }
      else
      {
        this.common.displayToaster("Vehicle Update Cancelled!");
      }
    });
    updateVehicleModal.present();
  }

  updateVehicleDetailsViaLogin(vehicleData, updatedVehicleData) {
    console.log(vehicleData);
    const prompt = this.alertCtrl.create({
      title: "NSDL Login Credentials",
      inputs: [
        {
          name: "username",
          type: "text",
          placeholder: "Username/GSTIN"
        },
        {
          name: "password",
          type: "password",
          placeholder: "Password"
        }
      ],
      buttons: [
        {
          text: "Cancel",
          cssClass: "cancelAlertButton",
          handler: data => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Login",
          cssClass: "submitAlertbutton",
          handler: data => {
            if (!data.username) {
              this.common.displayToaster("Username is required!");
              return false;
            }
            if (!data.password) {
              this.common.displayToaster("Password is required!");
              return false;
            } else {
              let userdata = {
                usergstin: this.common.selectedBusiness.GSTIN.gstin,
                username: data.username,
                password: data.password
              };
              console.log(userdata);
              this.common.displayLoader("Please wait...");
              this.api.post("generate-token", userdata).subscribe(
                res => {
                  console.log(res);
                  if (res["status"] == false) {
                    this.common.displayToaster(res["message"]);
                    this.common.hideLoader();
                  } else {
                    localStorage.setItem("AuthToken", JSON.stringify(res["token"]));
                    let date = new Date();
                    localStorage.setItem("TokenSaveTime", JSON.stringify(date));
                    let authToken = res["token"];
                    this.common.displayToaster("Auth Token Generated Successfully!");
                    this.common.hideLoader();
                    const prompt = this.alertCtrl.create({
                      title: "Login Success",
                      message: "Proceed for vehicle update!",
                      buttons: [
                        {
                          text: "Cancel",
                          cssClass: "cancelAlertButton",
                          handler: () => {
                            this.common.displayToaster(
                              "Vehicle Update Cancelled!"
                            );
                          }
                        },
                        {
                          text: "Update",
                          cssClass: "submitAlertButton",
                          handler: data => {
                            let updateData = {
                              userid: this.common.loginData.ID,
                              buid: this.common.selectedBusiness.buid,
                              gstinid: this.common.selectedBusiness.GSTIN
                                .gstinid,
                              inv_no: vehicleData.inv_no,
                              sek: authToken["sek"],
                              app_token: authToken["app_token"],
                              asp_secret_key: authToken["asp_secret_key"],
                              session_id: authToken["session_id"],
                              usergstin: userdata.usergstin,
                              authtoken: authToken["authtoken"],
                              vehicles: updatedVehicleData
                            };

                            console.log(updateData);

                            this.common.displayLoader("Please wait...");
                            this.api
                              .post("update-vehicle-ewb", updateData)
                              .subscribe(
                                res => {
                                  console.log(res);
                                  this.common.hideLoader();
                                  if (res["status"] == true) {
                                    this.common.displayToaster(
                                      "Vehicle Details Updated Successfully!"
                                    );
                                    const prompt = this.alertCtrl.create({
                                      title: "Updated",
                                      message: `Your vehicle details updated successfully!`,
                                      buttons: [
                                        {
                                          text: "GOT IT!",
                                          cssClass: "submitAlertButton",
                                          handler: data => {
                                            this.generatedBillList();
                                          }
                                        }
                                      ]
                                    });
                                    prompt.present();
                                  } else {
                                    this.common.displayToaster(res["message"]);
                                  }
                                },
                                err => {
                                  console.log(err);
                                  this.common.displayToaster(
                                    "Oops, Something went wrong!"
                                  );
                                  this.common.hideLoader();
                                }
                              );
                          }
                        }
                      ]
                    });
                    prompt.present();
                  }
                },
                err => {
                  console.log(err);
                  this.common.displayToaster("Oops, Something went wrong!");
                  this.common.hideLoader();
                }
              );
            }
          }
        }
      ]
    });
    prompt.present();
  }

  billDetails(value) {
    let userid = btoa(this.common.loginData.ID);
    let buid = btoa(this.common.selectedBusiness.buid);
    let gstinid = btoa(this.common.selectedBusiness.GSTIN.gstinid);
    let inv_no = parseFloat(value["inv_no"]);
    let isaction = "V";
    this.api
      .get(`get-ewb-details/${userid}/${buid}/${gstinid}/${inv_no}/${isaction}`)
      .subscribe(
        res => {
          if (res["status"] == false) {
            this.common.displayToaster(res["message"]);
          } else {
            let data = JSON.parse(atob(res["ewbDetails"]))[0];
            localStorage.setItem("billDetails", JSON.stringify(data));
            this.common.selected = data;
            this.common.selectedBillFrom = null;
            this.common.selectedBillTo = null;
            this.navCtrl.push("BillDetailsPage");
          }
          this.common.hideLoader();
        },
        err => {
          console.log(err);
          this.common.displayToaster("Oops, Something went wrong!");
          this.common.hideLoader();
        }
      );
  }

  generatedBillDetails(billData) {
    let userid = btoa(this.common.loginData.ID);
    let buid = btoa(this.common.selectedBusiness.buid);
    let gstinid = btoa(this.common.selectedBusiness.GSTIN.gstinid);
    let inv_no = parseFloat(billData["inv_no"]);
    let isaction = "V";
    this.api
      .get(`get-ewb-details/${userid}/${buid}/${gstinid}/${inv_no}/${isaction}`)
      .subscribe(
        res => {
          if (res["status"] == false) {
            this.common.displayToaster(res["message"]);
          } else {
            let data = JSON.parse(atob(res["ewbDetails"]))[0];
            localStorage.setItem("billDetails", JSON.stringify(data));
            this.common.selected = data;
            this.navCtrl.push("GeneratedBillDetailsPage");
          }
          this.common.hideLoader();
        },
        err => {
          console.log(err);
          this.common.displayToaster("Oops, Something went wrong!");
          this.common.hideLoader();
        }
      );
  }

  updateBusiness() {
    let data = {
      isBusiness: false,
      title_text: "Update Business",
      business_name: this.common.selectedBusiness.business_name,
      userid: this.common.loginData.ID,
      gstinid: 0,
      buid: 0,
      mode: 2,
      action: "U"
    };
    console.log(data);
    let addBusinessModal = this.modalCtrl.create("AddBusinessPage", data);
    addBusinessModal.onDidDismiss(data => {
      if (data.role != "cancel") {
        let id = btoa(this.common.loginData.ID);
        this.common.displayLoader("Please wait, We are preparing system...");
        this.api.get(`get-business-list/${id}`).subscribe(
          res => {
            this.common.hideLoader();
            localStorage.setItem("businessList", atob(res["businessList"]));
            let businessList = JSON.parse(atob(res["businessList"]));
            businessList.forEach(element => {
              if (element.buid == this.common.selectedBusiness.buid) {
                let matchedData = {
                  business_name: element.business_name,
                  buid: element.buid,
                  GSTIN: null
                };
                element.GSTIN.forEach(element => {
                  if (
                    parseInt(element.gstinid) ==
                    parseInt(this.common.selectedBusiness.GSTIN.gstinid)
                  ) {
                    matchedData.GSTIN = element;
                  }
                });
                this.common.selectedBusiness = matchedData;
                localStorage.setItem(
                  "selectedBusiness",
                  JSON.stringify(matchedData)
                );
              }
            });
          },
          err => {
            console.log(err);
            this.common.displayToaster("Oops, Something went wrong!");
            this.common.hideLoader();
          }
        );
      } else {
        this.common.displayToaster("Business Update Cancelled!");
      }
    });
    addBusinessModal.present();
  }

  calculatePrice(a, b, c, d, e) {
    return (
      parseFloat(a) +
      parseFloat(b) +
      parseFloat(c) +
      parseFloat(d) +
      parseFloat(e)
    ).toFixed(2);
  }
}
